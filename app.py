import pandas as pd
import numpy as np

#  Asosiy classni yaratib olamiz
class BaseNoteBook: 
  # Ota classning konstruktorini yaratamiz
  def __init__(self, fullName, birthDate):
    self._fullName = fullName
    self._birthDate = birthDate

# Voris classni yaratamiz   
class Notebook(BaseNoteBook):
  def __init__(self, fullName, birthDate, phoneNumber):
      super().__init__(fullName, birthDate)
      self._phoneNumber = phoneNumber
  result = ''
 
 #Table yaratish uchun pandas packagedan foydalanyapmiz
  df = pd.DataFrame()
  # Ma'lumot qo'shish uchun add metodini yaratamiz
  def add(self, tableData: pd.DataFrame):
       self.df = tableData
       df2 = pd.DataFrame([[self._fullName, self._birthDate, self._phoneNumber]], columns=['fullName', 'birthDate', 'phoneNumber']) 
       self.df = pd.concat([self.df, df2], ignore_index=True)
       print("Siz qaydni qo'shganingizdan so'ng table: ")
       return (self.df.head())
  # Ma'lumot o'chirish uchun delete metodini yaratamiz
  def delete(self, table: pd.DataFrame, ind):
        print(self.result)
        self.df = table
        return self.df.drop(index=ind)
   # Ma'lumot izlash uchun search metodini yaratamiz
  def search(self, table: pd.DataFrame, substring: str, case: bool = False):
        self.df = table
        data = self.df
        mask = np.column_stack([data[col].astype(str).str.contains(substring.lower(), case=case, na=False) for col in data])
        return (data.loc[mask.any(axis=1)])

# boshlang'ich holatda 3 ta rowli table yaratish uchun ma'lumotlar kiritib olaylik
people = {
  'fullName': ['Nigoraxon', 'Saodat', 'Muxlisa'],
  'birthDate': ['2000-08-12', '1999-03-10', '1999-01-25'],
  'phoneNumber': ['998972650323', '998909585658', '998934842444']
} 
# table yaratish uchun DataFrame funksiyasiga yaratilgan obyektimizni argument sifatida berib yuboramiz
tableData = pd.DataFrame(people)       
print(tableData)
isContinue = True

# Dastur qachon biz exit (0 bosmaguncha ya'ni dasturdan chiqmaguncha davom etadi)
while(isContinue):
      print('Dasturni davom ettirmoqchimisiz: 1: Ha 0: Yo\'q')
      isTrue = input('Tasdqilaysizmi: ')
      if(isTrue == str(1)):
          print('Qaysi amalni bajarishni istaysiz: \n 1: Qo\'shish (Add)  \n 2: O\'chirish (Delete)  \n 3: Qidirish (Search)')
          operation = input('Bajarmoqchi bo\'lgan amalingizni kiriting: ')
          if(operation == str(1)):
                print('Qo\'shmoqchi bo\'lganlaringizni quyidagi namuna asosida kiriting (probel tashlab)')
                print('Masalan : Laylo 1990-02-03 998902632569    Ism Tugilgan sana  Tel nomer')
                record = list(map(str, input().strip().split(' ')))
                 # Notebook classdan obyekt olamiz (bunda input datalarni construktor parametrlariga berib yuboramiz)
                note = Notebook(record[0], record[1], record[2])
                tableData = note.add(tableData)
                print(tableData)
          if(operation == str(2)):
                print(f'Qidirishdan oldingi holatda\n { tableData }')
                print("O'chirmoqchi bo'lgan qatoringiz index raqamini kiriting ")
                index = int(input())
                 # Notebook classdan obyekt olamiz boshlang'ich holatda biz faqat inputdan malumot berganimiz uchun bosh satr beryappan
                note = Notebook('','','')
                result = note.delete(tableData, index)
                print(result)
          if(operation == str(3)):
                print(f'Qidirishdan oldingi holatda\n { tableData }')
                print("Qidirmoqchi bolgan patternni kiriting: ")
                pattern = input()
                # Notebook classdan obyekt olamiz boshlang'ich holatda biz faqat inputdan malumot berganimiz uchun bosh satr beryappan
                note = Notebook('','','')
                result = note.search(tableData, pattern)
                print(result)
      else: 
        print('Dasturdan foydalanganingiz uchun tashakkur')
        isContinue = False





